import random
import string

from typing import Union, List, Optional, Iterator

from fastapi import FastAPI
from pydantic import BaseModel

app = FastAPI()


class Item(BaseModel):
    id: int
    name: str
    description: str
    price: float


class Next(BaseModel):
    after: str
    link: str


class Page(BaseModel):
    next: Next


class ItemResponse(BaseModel):
    results: Optional[List[Item]]
    paging: Optional[Page]

@app.get('/')
def read_root():
    return {"Hello": "World"}


@app.get("/items")
def read_item(after: Union[str, None] = None) -> ItemResponse:
    return generate_item_response(after)


def generate_item_response(after: Optional[str]) -> ItemResponse:
    return ItemResponse(
        results = list(generate_random_items(10)),
        paging = generate_random_page(after)
    )


def generate_random_items(length: int) -> Iterator[Item]:
    for i in range(length):
        yield Item(
            id=i,
            name=f"name{i}",
            description=f"some description for name{i}.",
            price=random.uniform(10.5, 75.5)
        )

def generate_random_page(after: str) -> Page:
    random_after = generate_random_string(8)
    next = Next(
        after=random_after,
        link=f"?after={random_after}"
    )
    return Page(next = next)


def generate_random_string(length: int) -> str:
    letters = string.ascii_lowercase
    result_str = "".join(random.choice(letters) for i in range(length))
    return result_str